# kubernetes-deployment

deployment of an spring boot application with kubernetes imperative approach



###pre-requisites

to reproduce you need to have installed kubernetes

specifically kubectl tool:

https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/

and minikube:

https://minikube.sigs.k8s.io/docs/start/

##How to run the application (declarative approach)

````
kubectl apply -f=master-deployment.yaml
````

##How to run the application (imperative approach)

you need to create a deployment with a custom image of this app stored on dockerhub

````
kubectl create deployment first-app --image=rsgarciae/kub-first-app
````

then you need to expose the deployment using a load balancer

````
kubectl expose deployment first-app --type=LoadBalancer --port=8081
````

once the service is created you can run this command to access the application

````
minikube service first-app
````

then a window of you browser will be opened, so just access the endpoint of the application /hello-world

#How to distribute applications traffic
To distribute the traffic of the application you need to scale the app creating some replicas like this 
````
kubectl scale deployment/first-app --replicas=3
````

##How to access swagger Ui

to access swagger use this endpoint

http://localhost:8081/swagger-ui/index.html#/
