package org.rsgarciae.kubernetesDeployment.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@CrossOrigin(origins = "*")
public class helloWorldController {

    @GetMapping(path = "/hello-world")
    public ResponseEntity<String> helloWorld() {
        String test = "hello world, this application now have swagger  ";
        log.info(String.format("******* %s :", test));
        return new ResponseEntity<String>(test, HttpStatus.OK);

    }

}
