package org.rsgarciae.kubernetesDeployment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan
@SpringBootApplication
public class KubernetesDeploymentApplication {
    public static void main(String[] args) {
        SpringApplication.run(KubernetesDeploymentApplication.class, args);
    }
}
