FROM openjdk:8-jdk-alpine
LABEL maintainer="Rsgarciae@gmail.com"

VOLUME /tmp

COPY ./target/kubernetes-deployment-1.0.0-SNAPSHOT.jar ./kubernetesDeployment.jar

EXPOSE 8081
CMD ["java","-jar","kubernetesDeployment.jar"]